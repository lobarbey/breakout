import pygame
import random
from constants import *
import math 
from Tile import Tile
import os 
import LevelEditor

class Game:

    def __init__(self,title):

        pygame.display.set_caption(title)

        self.levelEditor = None  #Pour la délégation lors de la phase d'édition


        self.width = DISPLAY_WIDTH
        self.heigth = DISPLAY_HEIGHT
        self.screen = pygame.display.set_mode((self.width,self.heigth))


        self.playbutton =  pg.transform.scale(pg.image.load(os.path.join("assets","Play.png")).convert().convert_alpha(),(MBUTTON_W,MBUTTON_H))
        self.playbutton_rect = pg.Rect(MBUTTON_X,PLAY_Y,MBUTTON_W,MBUTTON_H)


        self.lEdibutton =  pg.transform.scale(pg.image.load(os.path.join("assets","Ledi.png")).convert().convert_alpha(),(MBUTTON_W,MBUTTON_H))
        self.lEdibutton_rect = pg.Rect(MBUTTON_X,LEDI_Y,MBUTTON_W,MBUTTON_H)

        self.quitbutton =  pg.transform.scale(pg.image.load(os.path.join("assets","Quit.png")).convert().convert_alpha(),(MBUTTON_W,MBUTTON_H))
        self.quitbutton_rect = pg.Rect(MBUTTON_X,QUIT_Y,MBUTTON_W,MBUTTON_H)
        
        self.tiles = []

        for i in range(10):
            self.tiles.append(Tile(self.screen,i*(TILE_WIDTH+GAP),100,3));

        self.clock = pygame.time.Clock()
    
        self.state = MENU


    
    def getState(self):
        return self.state

    def processInputs(self):
        if self.state == EDITING_PHASE:
            self.levelEditor.processInputs()

            if self.levelEditor.getState() == GAME_STOP:
                self.state = MENU
                self.levelEditor = None
        else:
            for event in pygame.event.get():
                
                if event.type == pygame.QUIT:
                        self.state = GAME_STOP
                
                if event.type == pg.MOUSEBUTTONDOWN:
                    """Relâchement d'un bouton de la souris"""
                    
                    
                    pos = pg.mouse.get_pos() #Position du clic

                    x = pos[0] // (TILE_WIDTH + GAP)
                    y = pos[1] // (TILE_HEIGHT + GAP)

                    mouseStates = pg.mouse.get_pressed() #Etats de la souris (gauche,milieu,droit)

                    if mouseStates[0] and not mouseStates[1]:
                        
                        if self.playbutton_rect.collidepoint(pos[0],pos[1]):
                            self.state = GAME_PHASE
                            
                        elif self.lEdibutton_rect.collidepoint(pos[0],pos[1]):
                            self.levelEditor = LevelEditor.LevelEditor("Breakout!")
                            self.state = EDITING_PHASE

                        elif self.quitbutton_rect.collidepoint(pos[0],pos[1]) :
                            self.state = GAME_STOP



    def update_game(self):

        if self.state == EDITING_PHASE:
            self.levelEditor.update_game()
        else:
            self.screen.fill((0,0,0))
            if self.state == MENU:
                self.screen.blit(self.lEdibutton,self.lEdibutton_rect)
                self.screen.blit(self.quitbutton,self.quitbutton_rect)
                self.screen.blit(self.playbutton,self.playbutton_rect)

            elif self.state == GAME_PHASE:
                for tile in self.tiles:
                    tile.draw();


            self.clock.tick(FPS)
            pygame.display.flip()

    
        

