import pygame as pg
from Barre import Barre
from Tile import Tile

class Balle:

    def __init__(self,screen,x,y,width,height):

        self.img = pg.image.load("./assets/balle.png").convert_alpha()
        self.rect = self.img.get_rect()
        self.mask = pg.mask.from_surface(self.img)

        self.orient_x = 1
        self.orient_y = 1
        self.rect.x = x
        self.rect.y = y

        self.width = width
        self.height = height

        self.compteur = 0
        self.speed = 1

        self.screen = screen

    
    def moove(self):

        self.rect.x += self.orient_x
        self.rect.y += self.orient_y


    def cohésion_mur(self):

        if self.rect.y <= 0: #haut
            self.orient_y = 1
            self.compteur += 1
            
        if self.rect.x <= 0: #gauche
            self.orient_x = 1
            self.compteur += 1

        if self.rect.x + 14 >= 600: #droite
            self.orient_x = -1
            self.compteur += 1


    def cohésion_barre(self, barre):

        if self.rect.colliderect(barre.rect):
            self.compteur += 1

            if self.rect.y + 14 == barre.rect.y + 1: #haut
                self.orient_y = -1
        
            if self.rect.x + 14 == barre.rect.x + 1: #gauche
                self.orient_x = -1

            if self.rect.x == barre.rect.x + barre.width - 1: #droite
                self.orient_x = 1
            

    def cohésion_tile(self, tiles):

        hit = pg.sprite.spritecollideany(self, tiles) 

        if hit is not None:
            self.compteur += 1

            if self.rect.y + self.width == hit.y + 1: #haut
                self.orient_y = -1
                
            if hit.y + hit.height == self.rect.y + 1: #bas
                self.orient_y = 1

            if self.rect.x + self.width == hit.x + 1: #gauche
                self.orient_x = -1

            if hit.x + hit.width == self.rect.x + 1: #droite
                self.orient_x = 1

            hit.life -= 1
            if hit.life == 0:
                tiles.remove(hit)


    def up_speed(self):
        
        if self.compteur == 20 and self.speed < 6: #vitesse
            self.compteur = 0
            self.speed += 1


    def cohésion(self, barre, tiles):

        self.cohésion_mur()
        self.cohésion_barre(barre)
        self.cohésion_tile(tiles)
        self.up_speed()


    def end(self, tiles):

        if self.rect.y + 14 == 420:
            return 1

        elif not(tiles):
            return 2
            
        return 0
            

    def draw(self):

        self.screen.blit(self.img, pg.Rect(self.rect.x, self.rect.y, self.width, self.height))
