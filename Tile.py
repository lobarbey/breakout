import pygame as pg
from constants import *

class Tile(pg.sprite.Sprite):

    def __init__(self,screen,x,y,type):
        pg.sprite.Sprite.__init__(self);

        self.screen = screen


        #self.image = 
        self.width = TILE_WIDTH
        self.height = TILE_HEIGHT

        #self.rect = self.image.get_rect()


        self.x = x
        self.y = y

        self.type = type
        
        self.color = COLORS[self.type - 1]
        self.life = LIFE[self.type - 1]
    
    def draw(self):
        pg.draw.rect(self.screen,self.color,pg.Rect(self.x,self.y,self.width,self.height))

    def getType(self):
        return self.type

        
        

        