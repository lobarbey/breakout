import pygame as pg
import numpy as np
from constants import *
from Tile import Tile
from math import floor
import json
import os

class LevelEditor:

    def __init__(self,title):

        pg.display.set_caption(title)

        self.width = DISPLAY_WIDTH 
        self.heigth = DISPLAY_HEIGHT 
        
        self.screen = pg.display.set_mode((self.width,self.heigth))  #écran principal

        

        self.tiles = []  #L'ensemble des tuiles rendues à l'écran
        self.reprTiles = [] #le tableau représentatif des tuiles. Servira lors de la sauvegarde 

        #Initialisation des tableaux
        for _ in range(MAX_TILES_HOR):
            line = []
            line2 = []
            for _ in range(MAX_TILES_VERT):
                line.append(None)
                line2.append(0)

            self.tiles.append(line)
            self.reprTiles.append(line2)

        
        #l'holorge du jeu
        self.clock = pg.time.Clock()
    
        #Phase de jeu au début
        self.state = GAME_PHASE

        #Tableau des choix de tuiles disponibles
        self.choices = []
        
        for i in range(1,NUMBER_OF_TYPES_OF_TILES + 1):
            self.choices.append(Tile(self.screen,(i-1)*(TILE_WIDTH+GAP),self.heigth - TILE_HEIGHT,i))

        #Au début, aucun type de tuiles sélectionné
        self.selected = 0

        self.save_button = pg.transform.scale(pg.image.load(os.path.join("assets","saveButton.png")).convert().convert_alpha(),(100,40))
        self.button_rect = pg.Rect(DISPLAY_WIDTH-300-GAP,DISPLAY_HEIGHT-40-GAP,100,40)


        self.base_font = pg.font.Font(None, FONT_SIZE)
        self.save_filename = ''
        self.input_rect = pg.Rect(self.width - TILE_WIDTH - GAP - FONT_SIZE * 3,self.heigth - TILE_HEIGHT - GAP - FONT_SIZE // 2 ,140,32)
        self.active_input = False

    def save(self,filename):
        """Sauvegarde de l'état actuel dans filename"""
        with open(filename,"w") as f:
            json.dump(self.getReprTiles(),f,indent=4)

    def getReprTiles(self):
        return self.reprTiles

    def getState(self):
        """Retourne l'état du jeu actuel"""
        return self.state

    def processInputs(self):
        """Gestion des évènements"""
        
        for event in pg.event.get():
            
            if event.type == pg.QUIT:
                """Quitter l'éditeur"""
                self.state = GAME_STOP
                
            
            if event.type == pg.MOUSEBUTTONDOWN:
                """Relâchement d'un bouton de la souris"""
                
                
                pos = pg.mouse.get_pos() #Position du clic

                x = pos[0] // (TILE_WIDTH + GAP)
                y = pos[1] // (TILE_HEIGHT + GAP)

                mouseStates = pg.mouse.get_pressed() #Etats de la souris (gauche,milieu,droit)


                if mouseStates[0] and not mouseStates[2]:
                    """Gestion du clic gauche"""
                    if y == TOTAL_ROW - 1:  
                        """Clic sur la ligne du sélecteur"""
                        if x < 4  :
                            """Selection du type de tuile"""
                            self.selected = self.choices[x].getType()
                        elif self.button_rect.collidepoint(pos[0],pos[1]):
                            self.save(os.path.join("Levels",self.save_filename+".json"))
                            self.save_filename = ""
                        
                    
                    elif self.selected != 0 and y < MAX_TILES_VERT and self.reprTiles[x][y] == 0:
                        """Placement d'une tuile"""
                        self.tiles[x][y] = Tile(self.screen,x * (TILE_WIDTH + GAP),y*(TILE_HEIGHT+GAP),self.selected);
                        self.reprTiles[x][y] = self.selected

                    
                    
                elif mouseStates[2] and not mouseStates[0]:  
                    """Clic droit pour enlever des tuiles"""
                    if y < MAX_TILES_VERT and self.reprTiles[x][y] != 0:
                        self.tiles[x][y] = None
                        self.reprTiles[x][y] = 0
                
                if self.input_rect.collidepoint(pos[0],pos[1]):
                    self.active_input = True
                else:
                    self.active_input = False
                    
            if event.type == pg.KEYDOWN:
                if self.active_input:
                    if event.key == pg.K_BACKSPACE:
                        self.save_filename = self.save_filename[:-1]
                    else:
                        self.save_filename += event.unicode

    def update_game(self):
    
        self.screen.fill((0,0,0))

        

        if self.active_input :
            color = DARKISH_BLUE
        else:
            color = DARKISH_GREEN

        pg.draw.rect(self.screen, color, self.input_rect)
  
        text_surface = self.base_font.render(self.save_filename, True, (255, 255, 255))
        
        self.input_rect.w = max(100, text_surface.get_width()+10)
        self.screen.blit(text_surface, (self.input_rect.x+5, self.input_rect.y+5))

        text_surface = self.base_font.render("en tant que", True, (255,255,255))
        self.screen.blit(text_surface, (self.input_rect.x - 75, self.input_rect.y + 5))

        for line in self.tiles:
            for tile in line:
                if tile != None:
                    tile.draw()

        #Affichage des choix possibles
        for choice in self.choices:
            choice.draw()

        if self.selected != 0:
            """Mise en valeur du type de tuile sélectionné"""
            selectRect = pg.Rect((self.selected-1)*(TILE_WIDTH+GAP),self.heigth - TILE_HEIGHT - 5,TILE_WIDTH,TILE_HEIGHT + 10)
            pg.draw.rect(self.screen,COLORS[self.selected-1],selectRect)

        #Ligne de séparation
        pg.draw.line(self.screen,(255,255,255),(0,(MAX_TILES_VERT ) * (TILE_HEIGHT + GAP)),(self.width,(MAX_TILES_VERT) * (TILE_HEIGHT + GAP)))
        self.screen.blit(self.save_button ,self.button_rect)
        self.clock.tick(FPS)
        pg.display.flip()